import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (!!fighter) {
    const fighterDetails = fighterService.getFighterDetails(fighter._id);
    const fighterImage = createFighterImage(fighter);
    const fighterInfoList= createElement({
      tagName:'ul'
    });
    fighterInfoList.innerHTML=`
      <li>Name: ${fighter.name} </li>
      <li>Health: ${fighter.health} </li>
      <li>Attack: ${fighter.attack} </li>
      <li>Defense: ${fighter.defense} </li>
    `;
    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterInfoList);
  }
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
