import { controls } from '../../constants/controls';

let blockState = {};
let comboState = {};
let comboCooldown = {};
let firstFighterTotalHealth = 0;
let secondFighterTotalHealth = 0;

export async function fight(firstFighter, secondFighter) {
  blockState = {
    firstFighter: false, 
    secondFighter: false
  };
  comboState ={
    firstFighter: [false,false,false],
    secondFighter: [false,false,false]
  };
  comboCooldown ={
    firstFighter: false, 
    secondFighter: false
  };
  firstFighterTotalHealth = firstFighter.health;
  secondFighterTotalHealth = secondFighter.health;

  return new Promise((resolve) => {
    document.addEventListener('keyup', function (event) {
      switch (event.code) {
        case controls.PlayerOneAttack: 
          if (!blockState.firstFighter) {
            secondFighter.health -= getDamage(firstFighter, {
              ...secondFighter,
              block: blockState.secondFighter
            });
            changeHealthBar({
              ...secondFighter,
              key: 'secondFighter',
              totalHealth: secondFighterTotalHealth
            }); 
          }
          
        break;
  
        case controls.PlayerOneBlock: 
          blockState.firstFighter = false;
        break;
  
        case controls.PlayerTwoAttack: 
         if (!blockState.firstFighter) {
          firstFighter.health -= getDamage(secondFighter, {
            ...firstFighter,
            block: blockState.firstFighter
          });
          changeHealthBar({
            ...firstFighter,
            key: 'firstFighter',
            totalHealth: firstFighterTotalHealth
          });
        }
        break;

        case controls.PlayerTwoBlock:
          blockState.secondFighter = false;
        break;

        case controls.PlayerOneCriticalHitCombination[0]:
          comboState.firstFighter[0] = false;
        break;

        case controls.PlayerOneCriticalHitCombination[1]:
          comboState.firstFighter[1] = false;
        break;

        case controls.PlayerOneCriticalHitCombination[2]:
          comboState.firstFighter[2] = false;
        break;

        case controls.PlayerTwoCriticalHitCombination[0]:
          comboState.secondFighter[0] = false;
        break;

        case controls.PlayerTwoCriticalHitCombination[1]:
          comboState.secondFighter[1] = false;
        break;

        case controls.PlayerTwoCriticalHitCombination[2]:
          comboState.secondFighter[2] = false;
        break;
      }
      });

      document.addEventListener('keydown', function (event) {
        switch (event.code) {
          case controls.PlayerOneBlock: 
            blockState.firstFighter = true;
          break;

          case controls.PlayerTwoBlock:
            blockState.secondFighter = true;
          break;

          case controls.PlayerOneCriticalHitCombination[0]:
            comboState.firstFighter[0] = true;
          break;

          case controls.PlayerOneCriticalHitCombination[1]:
            comboState.firstFighter[1] = true;
          break;

          case controls.PlayerOneCriticalHitCombination[2]:
            comboState.firstFighter[2] = true;
          break;
          
          case controls.PlayerTwoCriticalHitCombination[0]:
            comboState.secondFighter[0] = true;
          break;

          case controls.PlayerTwoCriticalHitCombination[1]:
            comboState.secondFighter[1] = true;
          break;

          case controls.PlayerTwoCriticalHitCombination[2]:
            comboState.secondFighter[2] = true;
          break;
        }
        console.log(comboState.firstFighter);
        console.log(comboState.secondFighter);
        
        if (comboState.firstFighter.every((item) => item === true) && !comboCooldown.firstFighter && !blockState.firstFighter) {
          secondFighter.health -=getCriticalDamage({
            ...firstFighter,
            key: 'firstFighter'
          });
          changeHealthBar({
            ...secondFighter,
            key: 'secondFighter',
            totalHealth: secondFighterTotalHealth
          });
        }
        if (comboState.secondFighter.every((item) => item === true) && !comboCooldown.secondFighter && !blockState.secondFighter) {
          firstFighter.health -=getCriticalDamage({
            ...secondFighter,
            key: 'secondFighter'
          });
          changeHealthBar({
            ...firstFighter,
            key: 'firstFighter',
            totalHealth: firstFighterTotalHealth
          });
        }
        
        if (firstFighter.health <= 0) {
          resolve(secondFighter);
        } else if (secondFighter.health <= 0) {
          resolve(firstFighter);
        }
        console.log(secondFighter.health);
        console.log(firstFighter.health);
    // resolve the promise with the winner when fight is over
  });
})
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower >= hitPower || defender.block ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
 const attack = fighter.attack;
 const criticalHitChance = Math.random() + 1;
 return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}

export function getCriticalDamage(fighter){
  const criticalHit = fighter.attack * 2;
  comboCooldown[fighter.key] = true;
  setTimeout(() => {
    comboCooldown[fighter.key] = false;
  }, 10000 );
  return criticalHit;
}

function changeHealthBar(fighter){
  const healthBarWidth = fighter.health / (fighter.totalHealth / 100 );
  document.getElementById(fighter.key === 'firstFighter' ? 'left-fighter-indicator' : 'right-fighter-indicator').style.width = healthBarWidth + '%';

}
