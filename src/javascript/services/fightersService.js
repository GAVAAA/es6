import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    const endpoint = `details/fighter/${id}.json`;
    console.log(endpoint);
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    console.log(apiResult);
    return apiResult;
  }
}

export const fighterService = new FighterService();